var apiKey = "d9ba45a78ecaef311b6c0b02e4da4eb2";
var hkuMapId = "EIM-2dd72c8b-6372-41bd-9333-40298fa4b340";
var spMapId = "EIM-a94c096e-ee23-4e4a-86f6-8cd8764e578c";

var popup; // placeholder for current popup


var locations = [
    { name: "International Finance Center", latLng: [22.2859, 114.1581], tilt: 45, heading: 90, zoom: 15 },     // IFC
    { name: "CLP Power Hong Kong Limited", latLng: [22.30908, 114.19093], tilt: 45, heading: 180, zoom: 17},
    { name: "Tseung Kawn O", latLng: [22.31120, 114.25884], tilt: 45, heading: 270, zoom: 15},
    { name: "Hong Kong University", latLng: [22.2830, 114.1344], tilt: 45, heading: 180, zoom: 17 },     // HKU
    { name: "Hong Kong University", latLng: [22.28365, 114.13465], tilt: 0, heading: 180, zoom: 20 },     // HKU
    { name: "Hong Kong University", latLng: [22.2830, 114.1344], tilt: 45, heading: 180, zoom: 15 },     // HKU
    { name: "Hong Kong Science & Technology Park", latLng: [22.4261, 114.2107], tilt: 90, heading: 270, zoom: 15 },     // HKSTP
    { name: "Hong Kong Science & Technology Park (16E)", latLng: [22.42736, 114.21149], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 16E
    { name: "Hong Kong Science & Technology Park (5W)", latLng: [22.426485586429195, 114.21115931419327], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 5W
    { name: "Hong Kong Science & Technology Park (5E)", latLng: [22.42565817789622, 114.21390206432979], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 5E
    { name: "Hong Kong Science & Technology Park (2E)", latLng: [22.42859686830664, 114.20960251686755], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 2E 
    { name: "Hong Kong Science & Technology Park (1E)", latLng: [22.425308571308864, 114.2125849636986], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 1E 
    { name: "Hong Kong Science & Technology Park (6W)", latLng: [22.427776801132847, 114.20899229742801], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 6W 
    { name: "Hong Kong Science & Technology Park (3E)", latLng: [22.424594836340503, 114.21218663975756], tilt: 90, heading: 270, zoom: 15 },     // HKSTP 3E 
    { name: "Hong Kong Science & Technology Park (indoor map)", latLng: [22.427029, 114.209448], tilt: 0, heading: 270, zoom: 20 },
    { name: "Hong Kong Science & Technology Park", latLng: [22.4261, 114.2107], tilt: 90, heading: 270, zoom: 15 },     // HKSTP -END
];


var sequence = [ 
    () => { }, // Do Nothing
    () => {
        popupWithImage(map.getCenter(), "./weather.png", 280, 350, 280);
    },
    () => {  // Zoom to IFC
        popup.removeFrom(map);
        popup.closePopup();
        locationIndex++;
        startCrudeAnim(locationIndex, false);
    },
    () => {
        popupWithImage([22.28303, 114.158225], "./aqi.png", 499, 278, 499);
    },
    () => { // Zoom to HO
        popup.removeFrom(map);
        popup.closePopup();
        locationIndex++;
        startCrudeAnim(locationIndex);
    },
    () => { // Popup solar canvas
         var popupOptions = { 
            minWidth: 600,
            maxWidth: 600
         };
        popupWithImage(locations[locationIndex].latLng, "./sc.png", 600, 330, 600);
    },
    () => { // Zoom to TKO
        popup.removeFrom(map);
        popup.closePopup();
        locationIndex++;
        startCrudeAnim(locationIndex);
    },
    () => {
        popupWithImage(locations[locationIndex].latLng, "./beop.png", 600, 340, 600);
    },
    () => {
        popupWithImage(locations[locationIndex].latLng, "./beop2.png", 600, 340, 600);
    },
    () => { // Zoom to HKU
        popup.removeFrom(map);
        popup.closePopup();
        locationIndex++;
        startCrudeAnim(locationIndex);
    },
    () => { // Show SSA School
        popupWithImage(locations[locationIndex].latLng, "./ssa.png", 600, 330, 600);
    },
    () => {  // Enter HKU
        popup.removeFrom(map);
        popup.closePopup();
        map.indoors.enter(hkuMapId);    
    },
    () => { // Expand all floors
        map.indoors.expand();
    },
    () => { // Go up a floor
        var indoorMap = map.indoors.getActiveIndoorMap();
        if (indoorMap) {
            console.log("Floor count: " + indoorMap.getFloorCount());
            lerpToFloor(indoorMap, 1);
            console.log("Floor interpolation: " + map.indoors.getFloorInterpolation());
        }
    },
    () => { // Go up a floor
        var indoorMap = map.indoors.getActiveIndoorMap();
        if (indoorMap) {
            console.log("Floor count: " + indoorMap.getFloorCount());
            lerpToFloor(indoorMap, 2);
            console.log("Floor interpolation: " + map.indoors.getFloorInterpolation());
        }
    },
    () => { // Go up a floor
        var indoorMap = map.indoors.getActiveIndoorMap();
        if (indoorMap) {
            console.log("Floor count: " + indoorMap.getFloorCount());
            lerpToFloor(indoorMap, 3);
            console.log("Floor interpolation: " + map.indoors.getFloorInterpolation());
        }
    },
    () => { // Go up a floor
        var indoorMap = map.indoors.getActiveIndoorMap();
        if (indoorMap) {
            console.log("Floor count: " + indoorMap.getFloorCount());
            lerpToFloor(indoorMap, 4);
            console.log("Floor interpolation: " + map.indoors.getFloorInterpolation());
        }
    },
    () => { // Collapse expanded view - focus on floor 5
        map.indoors.collapse();
    },
    () => { // zoom in
        locationIndex++;
        var marker = L.marker(locations[locationIndex].latLng, {        
          title: "This is indoors!",
          indoorMapId: hkuMapId,
          indoorMapFloorId: 5
        }).addTo(map);      
        popup = marker.bindPopup("<img src='./ssa2.png' width=354 height=175 />", { minWidth: 354, maxWidth: 354 }).openPopup();
        map.setView(locations[locationIndex].latLng, locations[locationIndex].zoom, { tiltDegrees: locations[locationIndex].tilt });
    },
    () => { // zoom in
        popup.removeFrom(map);
        popup.closePopup();
        map.indoors.expand();
    },
    () => { // Exit HKU
        map.indoors.exit();
        locationIndex++;
        startCrudeAnim(locationIndex);
    },
    () => { // Zoom to HKSTP
        locationIndex++;
        startCrudeAnim(locationIndex, false);
    },
    () => { // Popup SP data
        popupWithText(locations[locationIndex].latLng, "<h1>Total: ~55GWh</h1><hr /><h2>230 MWh / Month per building</h2>");
    },
    () => { // 16E Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./16E.png", 260, 360, 260);
    },
    () => { // 5W Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./5W.png", 260, 360, 260);
    },
    () => { // 5E Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./5E.png", 260, 360, 260);
    },
    () => { // 2E Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./2E.png", 260, 360, 260);
    },
    () => { // 1E Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./1E.png", 260, 360, 260);
    },
    () => { // 6W Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./6W.png", 260, 360, 260);
    },
    () => { // 3E Solar
        locationIndex++;
        popupWithImage(locations[locationIndex].latLng, "./3E.png", 260, 360, 260);
    },
    () => {
        popup.removeFrom(map);
        popup.closePopup();
        locationIndex++;
        startCrudeAnim(locationIndex, false);
    },
    () => {
        popupWithImage(locations[locationIndex].latLng, "./nrg-comp.png", 656, 326, 656);
    },   
    () => { // Popup CLP SEC Logo 
         locationIndex++;
         startCrudeAnim(locationIndex, false);  
         popupWithImage(locations[locationIndex].latLng, "./sec-logo.jpg", 298, 124, 298);
    },
];
var locationIndex = -1;
var sequenceIndex = 0;
var marker;
var markerIds = [0, 1];
var map = L.Wrld.map("map", apiKey, { center: [22.29479, 114.17331], zoom: 12, indoorsEnabled: true, displayEntranceMarkers: false });
var entranceMarkers = [];

function popupWithImageIndoors(latLong, img, width, height, maxWidth) { 
         var popupOptions = { 
            minWidth: maxWidth,
            maxWidth: maxWidth
         };
         popup = L.popup(popupOptions)
                      .setLatLng(latLong)
                      .setContent(`<img src='${img}' width='${width}' height='${height}' />`)
                      .openPopup()
}

function popupWithImage(latLong, img, width, height, maxWidth) {
         var popupOptions = { 
            minWidth: maxWidth,
            maxWidth: maxWidth
         };
         popup = L.popup(popupOptions)
                      .setLatLng(latLong)
                      .setContent(`<img src='${img}' width='${width}' height='${height}' />`)
                      .openOn(map);
}

function popupWithText(latLong, txt) {
         popup = L.popup()
                      .setLatLng(latLong)
                      .setContent(txt)
                      .openOn(map);
}

// OK lerpToFloor and lerpTo are not pretty, but they work
function lerpToFloor(indoorMap, floor) {
    var floorInterp = floor / indoorMap.getFloorCount();
    var currentPos = map.indoors.getFloorInterpolation();
    lerpTo(currentPos, floorInterp, floor);
}

function lerpTo(currentPos, floorInterp, floor, step = 0.005, delay = 10) {
    setTimeout(() => {
        currentPos += step;
        map.indoors.setFloorInterpolation(currentPos);
        if(currentPos < floorInterp) {
            lerpTo(currentPos, floorInterp, floor);
        } else {
            map.indoors.setFloor(floor);
        }
    }, delay);
}

function startCrudeAnim(index, highlight=true) {
    console.log("Location index: " + index);
    map.setView(locations[index].latLng, locations[index].zoom, {
        headingDegrees: locations[index].heading,
        animate: true,
        durationSeconds:5,
        tiltDegrees: locations[index].tilt
    });

    if(highlight) {
        setTimeout(() => { highlightBuilding(index); }, 5000);
    }
}

function disableEntranceMarkers() {
    for(var x = 0; x < entranceMarkers.length; x++) {
        entranceMarkers[x].remove();
    }
}

function enableEntranceMarkers() {
    for(var x = 0; x < entranceMarkers.length; x++) {
        entranceMarkers[x].addTo(map);
    }
}

function highlightBuilding(index) {
    var buildingHighlight = L.Wrld.buildings.buildingHighlight(
            L.Wrld.buildings.buildingHighlightOptions()
            .highlightBuildingAtLocation(locations[index].latLng)
            .color([255, 255, 0, 128])
            );
    buildingHighlight.addTo(map);
    console.log(buildingHighlight.getBuildingInformation());
}

function precacheCompleteCallback(result) {
    console.log("Precache: " + result.getSucceeded());
}

function onIndoorMapLoad(event) {
    console.log("Indoor map loaded: " + event.indoorMapId);
    var readableName = map.indoors.tryGetReadableName(event.indoorMapId);
    console.log("readableName: " + readableName);
}

function onIndoorMapUnload(event) {
    console.log("Indoor map unloaded: " + event.indoorMapId);
}

function logKey(key) {
    if(key.code == "KeyL") {
        sequenceIndex++;
        checkLocationIndex();
        checkSequenceIndex();
        sequence[sequenceIndex]();
    }
    if(key.code == "KeyH") {
        sequenceIndex--;
        checkLocationIndex();
        checkSequenceIndex();
        sequence[sequenceIndex]();
    }
}

function checkSequenceIndex() {
    if(sequenceIndex > sequence.length -1) {
        sequenceIndex = 0;
    }
    if(sequenceIndex < 0) {
        sewuenceIndex = sequence.length -1;
    }
}

function checkLocationIndex() {
    return;  // DEBUG
    if(locationIndex > locations.length -1) {
        locationIndex = 0;
    }
    if(locationIndex < 0) {
        locationIndex = locations.length -1;
    }
}

function addCustomEntranceMarker(event) {
    var entrance = event.entrance;
    var marker = L.marker(entrance.getLatLng());
    
    marker.on("click", function() {
        if(map.indoors.isIndoors()) {
            map.indoors.exit();
        } else {
            map.indoors.enter(entrance);
        }
    });
    
    entranceMarkers.push(marker);
}


document.addEventListener('keydown', logKey);   

for(var x = 0; x < locations.length; x++) {
    console.log("Precaching location " + x + " (" + locations[x].latLng + ")");
    map.precacheWithDetailedResult(locations[x].latLng, 10000, precacheCompleteCallback);
}

map.indoors.on("indoormapload", onIndoorMapLoad);
map.indoors.on("indoormapunload", onIndoorMapUnload);
map.indoors.on("indoorentranceadd", addCustomEntranceMarker);
